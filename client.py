from socket import *
import os
import sys

import pika

from util.cmd import Cmd


WELCOME_MSG = "********************************\n" \
              "** Welcome to the BBS server. **\n" \
              "********************************"

HELP_MSG = "For command list, please type 'help'.\n" \
           "For command useage, type 'help [command]'."

PROMPT = "% "


if __name__ == "__main__":
    serverName = sys.argv[1]
    serverPort = int(sys.argv[2])
    clientSocket = socket(AF_INET, SOCK_STREAM)
    try:
        clientSocket.connect((serverName, serverPort))
    except socket.error as e:
        print(e)
        sys.exit(1)

    cmd = Cmd([""], clientSocket)
    print(WELCOME_MSG)
    print(HELP_MSG)
    while True:
        print(PROMPT, end = "")
        content = input()
        if not content.strip():
            continue
        else:
            cmd.args = content.split(' ')
            cmd.cmdType = Cmd.CmdType.from_str(cmd.args[0])
            response = cmd.exec()
            if response:
                print(response)
            else:
                print("Lost connection.")
                break
    clientSocket.close()
