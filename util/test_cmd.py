import pytest
import os
import sys

from cmd import *

current_dir = os.getcwd()
sys.path.append(current_dir)
#command for coverage pytest -p pytest_cov --cov
#100%

def test_mock_register(mocker):
    #mock the "create_table" method
    mocker.patch('cmd.create_table', return_value = True)
    
    t = Cmd(["register", "9999", "9999", "9999"], "9999")
    assert t._register() == True 
    #assert mock_create.called
    
    #create_table failed.
    mocker.patch('cmd.create_table', return_value = False)
    t = Cmd(["register", "9999", "9999", "9999"], "9999")
    assert t._register() == False 
    

def test_mock_login(mocker):
    #mock the "use_table" method
    mock_use = mocker.patch('cmd.use_table', return_value = True)
    
    t = Cmd(["login", "9999", "9999"], "9999")
    assert t._login() == True
    
    mock_use = mocker.patch('cmd.use_table', return_value = False)
    t2 = Cmd(["login", "9999", "9999"], "9999")
    assert t2._login() == False    

#if user didn't login, logout function won't executed
def test_mock_logout(mocker):
    r = "9999"
    testcase = ["login", r, r]
    #login success, logout success
    test = Cmd(testcase, r)
    
    #mock _login function, since we don't need to implement it here
    mock_method = mocker.patch.object(test, '_login')
    
    t = test._login()
    assert mock_method.called
    
    if t:
        assert test._logout() == True

    
def test_mail_to(mocker):
    p = "9999"
    
    #to satisfy the branch condition: 4 syntax errors
    #syntax error: len<3
    t = Cmd(["mail-to"], p)
    t.response = ""
    assert t._mail_to() == False
    
    #syntax error, no "--subject"
    t = Cmd(["mail-to", p, "--untitled", p], p)
    t.response = ""
    assert t._mail_to() == False
    
    #syntax error, no "--conent"
    t = Cmd(["mail-to", p, "--subject", p, "--untitled", p], p)
    t.response = ""
    assert t._mail_to() == False
    
    #non-login
    t = Cmd(["mail-to", p, "--subject", p, "--content" , p], p)
    t.response = ""
    mocker.patch('cmd.send_mail', return_value = False)
    assert t._mail_to() == False
    
    class mock_table():
        name = [p, p, p]
    m = mock_table()
    
    #success sent
    t.table = m
    mocker.patch('cmd.send_mail', return_value = True)
    assert t._mail_to() == True
    
    #recipent not existed
    mocker.patch('cmd.send_mail', return_value = False)
    assert t._mail_to() == False

def test_list_mail(mocker):
    p = "9999"
    
    t = Cmd(["list-mail"], p)
    t.response = ""
    mocker.patch('cmd.list_mail', return_value = [])
    assert t._list_mail() == False
    
    
    t = Cmd(["list-mail", p, "--subject", p, "--content", p], p)
    t.response = ""
    mocker.patch('cmd.use_table', return_value = True)
    t._login()
    mocker.patch('cmd.list_mail', return_value = [])
    assert t._list_mail() == True    

    
def test_retr_mail(mocker):
    p = "9999"
    t = Cmd(["retr-mail"], p)
    t.response = ""
    assert t._retr_mail() == False
    
    t = Cmd(["retr-mail", p], p)
    t.response = ""
    mocker.patch('cmd.retr_mail', return_value = False)
    assert t._retr_mail() == False
    
    t.table = p;
    mocker.patch('cmd.retr_mail', return_value = None)
    assert t._retr_mail() == False
    
    
    mock_mail = {
        'subject': "1",
        'from'   : "1",
        'date'   : "1",
        'content': "1",
    }
    mocker.patch('cmd.retr_mail', return_value = mock_mail)
    assert t._retr_mail() == True
    
    
def test_delete_mail(mocker):
    p = "9999"
    
    #syntax error
    t = Cmd(["delete-mail"], p)
    t.response = ""
    assert t._delete_mail() == False
    
    #non login
    t = Cmd(["delete-mail", p], p)
    t.response = ""
    assert t._delete_mail() == False
    
    #mail number not existed
    t.table = p
    mocker.patch('cmd.delete_mail', return_value = False)
    assert t._delete_mail() == False
    
    #success
    mocker.patch('cmd.delete_mail', return_value = True)
    assert t._delete_mail() == True
    


class MockTable:
    def __init__(self):
        self.name = "table_name"
    
    def put_item(self, **kwargs):
        return True
    
    def update_item(self, **kwargs):
        return True    
    
    def delete_item(self, **kwargs):
        return True
        
    def get_item(self, **kwargs):
        return {'Item':{'content':'<conent>'}}

def test_create_post(mocker):
    #mock the "_get_data" method, return postid
    mocker.patch('cmd.Cmd._get_data', return_value = "123")
    
    test_case = Cmd(["create-post", "<boardname>", "--title", "<title>", "--content", "<content>"], "9999")
    test_case.table = MockTable()
    assert test_case._create_post() == True
   
def test_read(mocker):
    #mock the "use_table" method, return MockTable object
    mocker.patch('cmd.use_table', return_value = MockTable())    
    
    test_case = Cmd(["read", "123"], "9999")
    test_case.response = "Author: Bob"
    assert test_case._read() == True

def test_delete_post():
    test_case = Cmd(["delete-post", "123"], "9999")
    test_case.table = MockTable()
    assert test_case._delete_post() == True

def test_update_post():
    test_case = Cmd(["update-post", "<boardname>", "123", "--title", "<new title>"], "9999")
    test_case.table = MockTable()
    assert test_case._update_post() == True
    
    test_case = Cmd(["update-post", "<boardname>", "123", "--content", "<new content>"], "9999")
    test_case.table = MockTable()
    assert test_case._update_post() == True

def test_comment(mocker):
    #mock the "_get_data" method, return author
    mocker.patch('cmd.Cmd._get_data', return_value = "author")
    
    #mock the "use_table" method, return MockTable object
    mocker.patch('cmd.use_table', return_value = MockTable())      
    
    test_case = Cmd(["comment", "123", "<comment>"], "9999")
    test_case.table = MockTable()
    assert test_case._comment() == True