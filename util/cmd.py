from enum import Enum
import time
import datetime
import sys

#from util.dynamodb import *
from dynamodb import *

BUFSIZE = 4096


class Cmd:
    
    
    class CmdType(Enum):
        register = "register"
        login = "login"
        logout = "logout"
        whoami = "whoami"
        exit = "exit"
        create_board = "create-board"
        create_post = "create-post"
        list_board = "list-board"
        list_post = "list-post"
        read = "read"
        delete_post = "delete-post"
        update_post = "update-post"
        comment = "comment"
        mail_to = "mail-to"
        list_mail = "list-mail"
        retr_mail = "retr-mail"
        delete_mail = "delete-mail"
        unknown = ""
        
        @staticmethod
        def from_str(label):
            try:
                cmdType = Cmd.CmdType(label)
                return cmdType
            except:
                return Cmd.CmdType.unknown
        
        
    client_side_cmd = [CmdType.register, CmdType.login, 
                        CmdType.logout, CmdType.create_post, 
                        CmdType.read, CmdType.delete_post, 
                        CmdType.update_post, CmdType.comment, 
                        CmdType.mail_to, CmdType.list_mail, 
                        CmdType.retr_mail, CmdType.delete_mail]
                        
    mail_cmd = [CmdType.mail_to, CmdType.list_mail, 
                 CmdType.retr_mail, CmdType.delete_mail]
    
    def __init__(self, args, socket):
        self.cmdType = Cmd.CmdType.from_str(args[0])
        self.args = args
        self.socket = socket
        self.table = ""
        self.response = ""
    
    def exec(self):
        content = ' '.join(self.args)
        if self.cmdType not in Cmd.mail_cmd and self.cmdType:
            self.socket.send(content.encode())
            time.sleep(0.1)
            self.response = self.socket.recv(BUFSIZE).decode()
        else:
            self.response = '[0]'

        if self.cmdType in Cmd.client_side_cmd and self.response[1] == '0':
            result = getattr(Cmd, "_"+self.cmdType.name)(self)
            
        if len(self.response) > 3:
            return self.response[3:]
        else:
            return self.response
    
    def _get_data(self):
        self.socket.send("0".encode())
        time.sleep(0.1)
        return self.socket.recv(BUFSIZE).decode()
    
    def _register(self):
        return create_table(self.args[1])
    
    def _login(self):
        self.table = use_table(self.args[1])
        if self.table:
            return True
        else:
            return False
            
    def _logout(self):
        self.table = ""
        return True
           
    def _create_post(self):
        postid = self._get_data()
        content = ' '.join(self.args[self.args.index("--content")+1:]).replace("<br>", "\n")
        self.table.put_item(
            Item={
                'type': 'POST#',
                'meta': postid,
                'content': content
            }
        )
        return True
        
    def _read(self):
        table = use_table(self.response.split(' ')[1])
        result = table.get_item(Key={"type": "POST#", "meta": self.args[1]})
        self.response += result['Item']['content'] + "\n--\n"
        if "comments" in result['Item']:
            for comment in result['Item']['comments']:
                self.response += comment
        return True
        
    def _delete_post(self):
        self.table.delete_item(
            Key={
                'type': 'POST#',
                'meta': self.args[1]
            }
        )
        return True
        
    def _update_post(self):
        if self.args[2] == "--content":
            content = ' '.join(self.args[3:]).replace("<br>", "\n")
            self.table.update_item(
                Key={
                    'type': 'POST#',
                    'meta': self.args[1]
                },
                UpdateExpression='SET content = :val1',
                ExpressionAttributeValues={
                    ':val1': content
                }
            )
        return True
    
    def _comment(self):
        author = self._get_data()
        table = use_table(author)
        table.update_item(
            Key={
                'type': 'POST#',
                'meta': self.args[1]
            },
            UpdateExpression='SET comments = list_append(if_not_exists(comments, :empty_list), :i)',
            ExpressionAttributeValues={
                ':empty_list': [''],
                ':i': [self.table.name[3:]+': '+self.args[2]+'\n']
            }
        )
        return True
       
    def _mail_to(self):
        if len(self.args) < 3 or self.args.count("--subject")==0 or self.args.count("--content")==0:
            self.response += "Usage: mail-to <username> --subject <subject> --content <content>\n"
            return False
        if self.table:
            result = send_mail(
                table_name = self.args[1],
                subject = ' '.join(self.args[3:self.args.index("--content")]),
                content = ' '.join(self.args[self.args.index("--content")+1:]),
                author = self.table.name[3:]
            )
            if result:
                self.response += "Sent successfully.\n"
                return True
            else:
                self.response += self.args[1]+" does not exist.\n"
                return False
        else:
            self.response += "Please login first.\n"
            return False
            
    def _list_mail(self):
        if self.table:
            emails = list_mail(self.table)
            id = 1
            self.response += 'ID\tSubject\t\tFrom\tDate\n'
            for email in emails:
                self.response += str(id)+'\t'+email['subject']+'\t' \
                                +email['from']+'\t' \
                                +email['date'][2:4]+'/'+email['date'][4:6]+'\n'
                id += 1
            return True
        else:
            self.response += "Please login first.\n"
            return False
            
    def _retr_mail(self):
        if len(self.args) != 2:
            self.response += "Usage: retr-mail <mail#>"
            return False
        if self.table:
            email = retr_mail(self.table, int(self.args[1]))
            if email:
                self.response += "Subject\t: "+email['subject']+'\n'\
                                +"From\t: "+email['from']+'\n' \
                                +"Date\t: 20"+email['date'][0:2]+'-' \
                                +email['date'][2:4]+'-'+email['date'][4:6]+'\n'\
                                +"--\n" \
                                +email['content']+'\n'
                return True
            else:
                self.response += "No such mail.\n"
                return False
        else:
            self.response += "Please login first.\n"
            return False

    def _delete_mail(self):
        if len(self.args) != 2:
            self.response += "Usage: delete-mail <mail#>"
            return False
        if self.table:
            result = delete_mail(self.table, int(self.args[1]))
            if not result:
                self.response += "No such mail.\n"
                return False
            else:
                self.response += "Mail deleted.\n"
                return True
        else:
            self.response += "Please login first.\n"
            return False
