import time
import datetime
import logging

import boto3
from botocore.exceptions import ClientError


def create_table(table_name, region=None):
    print('create is called.')
    
    try:
        if region is None:
            dynamodb = boto3.client('dynamodb')
        else:
            dynamodb = boto3.client('dynamodb', region_name=region)
        # Create the DynamoDB table.
        table = dynamodb.create_table(
            TableName = '000'+table_name,
            KeySchema=[
                {
                    'AttributeName': 'type',
                    'KeyType': 'HASH'
                },
                {
                    'AttributeName': 'meta',
                    'KeyType': 'RANGE'
                }
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': 'type',
                    'AttributeType': 'S'
                },
                {
                    'AttributeName': 'meta',
                    'AttributeType': 'S'
                },
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )

    except ClientError as e:
        logging.error(e)
        return False

    # Wait until the table exists.
    dynamodb.get_waiter('table_exists').wait(TableName='000'+table_name,WaiterConfig={
        'Delay': 1,
        'MaxAttempts': 60
    })
    return True


def use_table(table_name, region=None):
    try:
        if region is None:
            dynamodb = boto3.resource('dynamodb')
        else:
            dynamodb = boto3.resource('dynamodb', region_name=region)
        
        table = dynamodb.Table('000'+table_name)
    except ClientError as e:
        logging.error(e)
        return False
    return table

def send_mail(table_name, subject, content, author):
    try:
        dynamodb_client = boto3.client('dynamodb')
        dynamodb_client.describe_table(TableName='000'+table_name)
        table = use_table(table_name)
        table.put_item(
            Item={
                'type': 'MAIL#',
                'meta': str(time.time()),
                'subject': subject,
                'content': content,
                'from': author,
                'date': datetime.date.today().strftime("%y%m%d")
            }
        )
        return True
    except dynamodb_client.exceptions.ResourceNotFoundException:
        return False

def list_mail(table):
    dynamodb = boto3.resource('dynamodb')
    result = table.scan(
        FilterExpression=boto3.dynamodb.conditions.Attr('type').eq('MAIL#')
    )
    return result['Items']

def retr_mail(table, index):
    dynamodb = boto3.resource('dynamodb')
    result = table.scan(
        FilterExpression=boto3.dynamodb.conditions.Attr('type').eq('MAIL#')
    )
    if len(result['Items']) < index:
        return False
    else:
        email = result['Items'][index-1]
        return email    

def delete_mail(table, index):
    dynamodb = boto3.resource('dynamodb')
    result = table.scan(
        FilterExpression=boto3.dynamodb.conditions.Attr('type').eq('MAIL#')
    )
    if len(result['Items']) < index:
        return False
    else:
        email = result['Items'][index-1]
        table.delete_item(
            Key={
                'type': 'MAIL#',
                'meta': email['meta']
            }
        )
        return True
